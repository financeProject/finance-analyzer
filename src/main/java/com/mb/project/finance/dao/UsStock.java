package com.mb.project.finance.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UsStock {

    @JsonProperty
    private String customer;

    @JsonProperty
    private String ccyPair;

    @JsonProperty
    private String type;

    @JsonProperty
    private String style;

    @JsonProperty
    private String direction;

    @JsonProperty
    private String strategy;

    @JsonProperty
    private String tradeDate;

    @JsonProperty
    private Long amount1;

    @JsonProperty
    private Long amount2;

    @JsonProperty
    private float rate;

    @JsonProperty
    private String deliveryDate;

    @JsonProperty
    private String expiryDate;

    @JsonProperty
    private String excerciseStartDate;

    @JsonProperty
    private String payCcy;

    @JsonProperty
    private String premium;

    @JsonProperty
    private String premiumCcy;

    @JsonProperty
    private String premiumType;

    @JsonProperty
    private String premiumDate;

    @JsonProperty
    private String legalEntity;

    @JsonProperty
    private String trader;
}
