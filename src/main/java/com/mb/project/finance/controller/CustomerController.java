package com.mb.project.finance.controller;


import com.mb.project.finance.dao.EuropeStock;
import com.mb.project.finance.dao.UsStock;
import com.mb.project.finance.service.StockDemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/stock")
public class CustomerController {

    private final StockDemoService stockDemoService ;

    @Autowired
    public CustomerController(StockDemoService stockDemoService) {
        this.stockDemoService = stockDemoService;
    }

    @RequestMapping(value = "/EuropeStock", method = RequestMethod.GET)
    public List<EuropeStock> getEuropeanStock() {
      log.info("get European stock");
        return stockDemoService.getEuropeanStockFromFile();
    }


    @RequestMapping(value = "/UsStock", method = RequestMethod.GET)
    public List<UsStock> getUsStock() {
        log.info("Get US Stock");
        return stockDemoService.getUSStockFromFile();
    }

}
