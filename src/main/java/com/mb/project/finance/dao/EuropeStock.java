package com.mb.project.finance.dao;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class EuropeStock {

    @JsonProperty
    private String customer;
//
    @JsonProperty
    private String ccyPair;

    @JsonProperty
    private String type;

    @JsonProperty
    private String direction;

    @JsonProperty
    private String tradeDate;

    @JsonProperty
    private Long amount1;

    @JsonProperty
    private Long amount2;

    @JsonProperty
    private float rate;

    @JsonProperty
    private String valueDate;

    @JsonProperty
    private String legalEntity;

    @JsonProperty
    private String trader;
}
