package com.mb.project.finance.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mb.project.finance.dao.EuropeStock;

import com.mb.project.finance.dao.UsStock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


@Slf4j
@Service
public class StockDemoService {

    private static final String STOCK_JSON_FILE_PATH = "src/main/resources/sample.json";

    private final ObjectMapper objectMapper;

    @Autowired
    public StockDemoService(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public List<EuropeStock> getEuropeanStockFromFile() {
        List<EuropeStock> europeStock = null;
        try {
            europeStock = getStock();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("stock does not exist");
        }
        log.info("return correct stocks");
        return europeStock;
    }

    public List<UsStock> getUSStockFromFile() {
        List<UsStock> usStock = null;
        try {
            usStock = getUSStock();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("stock does not exist");
        }
        log.info("return correct stocks");
        return usStock;
    }


    private List<EuropeStock> getStock() throws IOException {
        return objectMapper.readValue(new FileReader(STOCK_JSON_FILE_PATH), new TypeReference<List<EuropeStock>>() {
        });
    }

    private List<UsStock> getUSStock() throws IOException {
        return objectMapper.readValue(new FileReader(STOCK_JSON_FILE_PATH), new TypeReference<List<UsStock>>() {
        });
    }

}
